﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innovandalism.SteamWebApi.Helpers
{
    public static class GUID
    {
        public static long GetLongGUID()
        {
            Guid uniqueID = Guid.NewGuid();
            byte[] bArr = uniqueID.ToByteArray();
            return BitConverter.ToInt64(bArr, 0);
        }
    }
}
