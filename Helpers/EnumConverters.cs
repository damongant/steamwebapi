﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Innovandalism.SteamWebApi.BaseAPI;

namespace Innovandalism.SteamWebApi.Helpers
{
    public static class EnumConverters
    {
        public static MessageType ConvertMessageType(string messageType)
        {
            switch (messageType)
            {
                case "typing":
                    return MessageType.Typing;
                case "leftconversation":
                    return MessageType.LeftConversation;
                case "saytext":
                    return MessageType.SayText;
                case "personastate":
                    return MessageType.PersonaState;
                default:
                    return MessageType.Unknown;
            }
        }

        public static SteamPersonaState ConvertSteamUserState(int i)
        {
            return (SteamPersonaState)i;
        }
    }
}
