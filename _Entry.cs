﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Innovandalism.SteamWebApi.BaseAPI;

namespace Innovandalism.SteamWebApi
{
    /// <summary>
    /// Just kill this file and compile as DLL if you need it, I just don't want to create a whole new project for this.
    /// </summary>
    class _Entry
    {
        public static void Main()
        {
            try
            {
                Steam steamEngine = new Steam();

                Console.WriteLine("Todays UMQID is {0}", steamEngine.UMQID);

                Console.Write("Username: ");
                string username = Console.ReadLine();
                Console.Write("Password: ");
                string password = Console.ReadLine();

                Console.WriteLine("Authentificating...");
                SteamWebCredentials userInfo;
                try
                {
                    userInfo = steamEngine.ISteamOAuth2.GetTokenWithCredentials(username, password);
                }
                catch (SteamWebException ex)
                {
                    if (ex.Code != "steamguard_code_required")
                        throw ex;
                    Console.WriteLine("- SteamGuard code required -");
                    Console.Write("Code: ");
                    string gc = Console.ReadLine();
                    userInfo = steamEngine.ISteamOAuth2.GetTokenWithCredentials(username, password, gc);
                }
                Console.WriteLine("Logging you in...");
                steamEngine.ISteamWebUserPresenceOauth.Logon(userInfo.AccessToken);

                while (true)
                {
                    try
                    {
                        Console.WriteLine("Polling status...");
                        var f = steamEngine.ISteamWebUserPresenceOauth.Poll(userInfo.AccessToken);
                        foreach (Message m in f.Messages)
                        {
                            if (m.Type == MessageType.SayText)
                            {
                                var message = (BaseAPI.Messages.SayTextMessage)m;
                                steamEngine.ISteamWebUserPresenceOauth.Message(MessageType.SayText, message.SteamIDFrom.ToString(), message.Text);
                            }
                        }
                        System.Threading.Thread.Sleep(1);
                    }
                    catch (SteamWebException ex)
                    {
                        Console.WriteLine(ex.Code);
                    }
                }
            }
            catch (Innovandalism.SteamWebApi.BaseAPI.SteamWebException ex)
            {
                Console.WriteLine("Exception: {0}", ex.Code);
            }
            Console.ReadKey();
        }
    }
}
