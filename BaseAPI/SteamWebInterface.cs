﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Web;
using System.IO;
using System.Xml.Linq;
using System.Diagnostics;

namespace Innovandalism.SteamWebApi.BaseAPI
{
    /// <summary>
    /// Cool base class with netcode (hui HTTP)
    /// </summary>
    public abstract class SteamWebInterface
    {
        public SteamWebInterface(Steam parentEngine)
        {
            engine = parentEngine;
        }

        private Steam engine;

        public Steam Engine
        {
            get { return engine; }
        }

        public virtual string InterfaceName
        {
            get
            {
                return null;
            }
        }

        protected HttpWebRequest SetupRequest(string method, int methodVersion, string httpMethod, Dictionary<string, string> parameters)
        {
            
            string requestBodyString = "";
            if (parameters.ContainsKey("format"))
                parameters["format"] = "xml";
            else
                parameters.Add("format", "xml");

            foreach (KeyValuePair<string, string> kvp in parameters) 
            {
                if (requestBodyString != "")
                    requestBodyString += "&";
                requestBodyString += String.Format("{0}={1}", kvp.Key, HttpUtility.UrlEncodeUnicode(kvp.Value));
            }

            var uri = String.Format("https://{0}/{1}/{2}/v{3}", new object[] { Engine.ApiServer, InterfaceName, method, methodVersion.ToString().PadLeft(4, '0')} );
            if (httpMethod == "GET") 
            {
                uri += "?" + requestBodyString;
            }

            var webRequest = (HttpWebRequest)HttpWebRequest.Create(uri);

            webRequest.Host = "api.steampowered.com:443";
            webRequest.UserAgent = "Steam App / Android / 1.0 / 1297579";
            webRequest.Method = httpMethod;
            webRequest.KeepAlive = true;
            webRequest.ProtocolVersion = HttpVersion.Version11;
            webRequest.ContentType = "application/x-www-form-urlencoded";

            if (webRequest.Method == "POST")
            {
                var reqestBody = Encoding.UTF8.GetBytes(requestBodyString);
                webRequest.ContentLength = reqestBody.Length;
                webRequest.GetRequestStream().Write(reqestBody, 0, reqestBody.Length);
            }
            return webRequest;
        }

        protected XElement DoRequest(HttpWebRequest webRequest)
        {
            var httpResponse = webRequest.GetResponse();
            var response = XDocument.Load(httpResponse.GetResponseStream());

            if (response.DocumentType.Name == "response")
            {
                var responseElement = response.Element("response");

                if (responseElement.Element("error") != null)
                {
                    if (responseElement.Element("error").Value.ToLower() == "ok")
                        return responseElement;
                    if (responseElement.Element("x_errorcode") != null)
                        throw new SteamWebException(responseElement.Element("x_errorcode").Value, responseElement.Value);
                    throw new SteamWebException(responseElement.Element("error").Value, responseElement.Value);
                }
                return responseElement;
                
            }
            else if (response.DocumentType.Name == "friendslist") 
            {
                //Never encountered an error, should maybe try invalid access token? --Nevermind, throws at GetResponse with HTTP 401
                return response.Element("friendslist");
            }
            else
                throw new NotImplementedException();
        }
    }
}
