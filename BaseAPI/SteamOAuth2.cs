﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Xml.Linq;

namespace Innovandalism.SteamWebApi.BaseAPI
{
    /// <summary>
    /// Provides OAuth (wat?) authentification, sometimes even with x_webcookie (not any longer as of 03/06/12)
    /// </summary>
    public class SteamOAuth2 : SteamWebInterface
    {
        public SteamOAuth2(Steam parent) : base(parent) { }

        public override string InterfaceName
        {
            get
            {
                return "ISteamOAuth2";
            }
        }

        public SteamWebCredentials GetTokenWithCredentials(string username, string password)
        {
            return GetTokenWithCredentials(username, password, null);
        }

        public SteamWebCredentials GetTokenWithCredentials(string username, string password, string guardToken)
        {
            var p = new Dictionary<string, string>();
            p.Add("client_id", Engine.ClientID);
            p.Add("grant_type", "password");
            p.Add("username", username);
            p.Add("password", password);
            p.Add("scope", "read_profile write_profile read_client write_client");
            if (guardToken != null)
                p.Add("x_emailauthcode", guardToken);

            var request = SetupRequest("GetTokenWithCredentials", 1, "POST", p);

            var response = DoRequest(request);

            Engine.AccessToken = response.Element("access_token").Value;

            return new SteamWebCredentials()
            {
                AccessToken = response.Element("access_token").Value,
                TokenType = response.Element("token_type").Value,
                SteamID = response.Element("x_steamid").Value // TODO: x_* implies, this is not a requirement, cleanup code to check if actually supplied
            };
        }
    }
}
