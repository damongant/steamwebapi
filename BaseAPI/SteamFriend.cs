﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innovandalism.SteamWebApi.BaseAPI
{
    /// <summary>
    /// Steam Friend as returned by ISteamUserOAuth/GetFriendsList
    /// </summary>
    public class SteamFriend
    {
        string steamID;
        public string SteamID
        {
            get { return steamID; }
            set { steamID = value; }
        }
        
        string relationship;
        public string Relationship
        {
            get { return relationship; }
            set { relationship = value; }
        }
        
        DateTime friendSince;
        public DateTime FriendSince
        {
            get { return friendSince; }
            set { friendSince = value; }
        }
    }
}
