﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innovandalism.SteamWebApi.BaseAPI
{
    public class Steam
    {
        public Steam()
        {
            umqid = Innovandalism.SteamWebApi.Helpers.GUID.GetLongGUID();
        }

        #region PROPERTIES
        string apiServer = "api.steampowered.com";

        public string ApiServer
        {
            get { return apiServer; }
            set { apiServer = value; }
        }

        string clientID = "DE45CD61"; //Public Universe

        public string ClientID
        {
            get { return clientID; }
            set { clientID = value; }
        }

        long umqid;

        public long UMQID
        {
            get { return umqid; }
            set { umqid = value; }
        }

        private int lastMessage = 0;

        public int LastMessage
        {
            get { return lastMessage; }
            set { lastMessage = value; }
        }

        public string LastMessageStr
        {
            get { return lastMessage.ToString(); }
            set { lastMessage = int.Parse(value); }
        }

        string accessToken;

        public string AccessToken
        {
            get 
            {
                if (accessToken == null)
                    throw new SteamWebException("access_token_required", "We stopped a request from happening, get a code first");
                return accessToken; 
            }
            set { accessToken = value; }
        }
        #endregion

        #region INTERFACES
        SteamOAuth2 iSteamOAuth2;

        public SteamOAuth2 ISteamOAuth2
        {
            get 
            {
                if (iSteamOAuth2 == null)
                    iSteamOAuth2 = new SteamOAuth2(this);
                return iSteamOAuth2;
            }
        }

        SteamWebAPIUtil iSteamWebAPIUtil;

        public SteamWebAPIUtil ISteamWebAPIUtil
        {
            get 
            {
                if (iSteamWebAPIUtil == null)
                    iSteamWebAPIUtil = new SteamWebAPIUtil(this);
                return iSteamWebAPIUtil;
            }
        }

        SteamWebUserPresenceOAuth iSteamWebUserPresenceOauth;

        public SteamWebUserPresenceOAuth ISteamWebUserPresenceOauth
        {
            get 
            {
                if (iSteamWebUserPresenceOauth == null)
                    iSteamWebUserPresenceOauth = new SteamWebUserPresenceOAuth(this);
                return iSteamWebUserPresenceOauth;
            }
        }

        SteamUserOAuth iSteamUserOAuth;

        public SteamUserOAuth ISteamUserOAuth
        {
            get 
            {
                if (iSteamWebUserPresenceOauth == null)
                    iSteamWebUserPresenceOauth = new SteamWebUserPresenceOAuth(this);
                return iSteamUserOAuth; 
            }
        }
        #endregion
    }
}
