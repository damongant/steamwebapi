﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innovandalism.SteamWebApi.BaseAPI
{
    /// <summary>
    /// Credentials as returned by ISteamOauth2
    /// </summary>
    public class SteamWebCredentials
    {
        private string accessToken;
        public string AccessToken
        {
            get { return accessToken; }
            set { accessToken = value; }
        }

        private string tokenType;
        public string TokenType
        {
            get { return tokenType; }
            set { tokenType = value; }
        }

        private string steamID;
        public string SteamID
        {
            get { return steamID; }
            set { steamID = value; }
        }
        
        private string webCookie;
        public string WebCookie
        {
            get { return webCookie; }
            set { webCookie = value; }
        }
    }
}
