﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innovandalism.SteamWebApi.BaseAPI
{
    public abstract class Message
    {
        MessageType type;

        internal MessageType Type
        {
            get { return type; }
            set { type = value; }
        }

        DateTime timestamp;

        public DateTime Timestamp
        {
            get { return timestamp; }
            set { timestamp = value; }
        }

        long steamIDFrom;

        public long SteamIDFrom
        {
            get { return steamIDFrom; }
            set { steamIDFrom = value; }
        }
    }
}
