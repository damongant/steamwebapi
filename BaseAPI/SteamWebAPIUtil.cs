﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innovandalism.SteamWebApi.BaseAPI
{
    /// <summary>
    /// Useless interface to get the servertime, don't ask why but it returns "servertime" as unixtime (UTC) and "servertimestring" as formated string (EST)
    /// </summary>
    public class SteamWebAPIUtil : SteamWebInterface
    {
        public SteamWebAPIUtil(Steam parent) : base(parent) { }

        public override string InterfaceName
        {
            get
            {
                return "ISteamWebAPIUtil";
            }
        }

        /// <summary>
        /// Returns the server time (UTC).
        /// </summary>
        /// <returns></returns>
        public DateTime GetServerInfo()
        {
            var p = new Dictionary<string, string>();

            var request = SetupRequest("GetServerInfo", 1, "GET", p);

            var response = DoRequest(request);
            
            DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Epoch.AddSeconds(double.Parse(response.Element("servertime").Value));
        }
    }
}
