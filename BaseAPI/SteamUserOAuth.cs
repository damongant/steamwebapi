﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Innovandalism.SteamWebApi.BaseAPI
{
    /// <summary>
    /// Mainly unfinished interface for getting information about yourself/friends/groups
    /// </summary>
    public class SteamUserOAuth : SteamWebInterface
    {
        public SteamUserOAuth(Steam parent) : base(parent) { }

        public override string InterfaceName
        {
            get
            {
                return "ISteamUserOAuth";
            }
        }

        public IEnumerable<SteamFriend> GetFriendList(string accessToken, string forSteamID)
        {
            var p = new Dictionary<string, string>();
            p.Add("access_token", accessToken);
            p.Add("steamid", forSteamID);

            var request = SetupRequest("GetFriendList", 1, "GET", p);

            var response = DoRequest(request);

            foreach (XElement friend in response.Element("friends").Elements("friend"))
            {
                DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                
                yield return new SteamFriend()
                {
                    SteamID = friend.Element("steamid").Value,
                    Relationship = friend.Element("relationship").Value,
                    FriendSince = Epoch.AddSeconds(double.Parse(friend.Element("friend_since").Value))
                };
            }
        }

        public void GetUserSummaries(string accessToken, List<SteamFriend> friendsToLookup) 
        {
#if DEBUG
            string steamIDs = "";
            foreach (SteamFriend friend in friendsToLookup)
            {
                if (steamIDs != "")
                    steamIDs += ",";
                steamIDs += friend.SteamID;
            }

            var p = new Dictionary<string, string>();
            p.Add("access_token", accessToken);
            p.Add("steamids", steamIDs);

            var request = SetupRequest("GetUserSummaries", 1, "GET", p);
            var response = DoRequest(request); 
#else
            throw new NotImplementedException();
#endif
        }

        public void GetGroupList(string accessToken, string steamID)
        {
#if DEBUG
            var p = new Dictionary<string, string>();
            p.Add("access_token", accessToken);
            p.Add("steamid", steamID);

            var request = SetupRequest("GetGroupList", 1, "GET", p);
            var response = DoRequest(request);
#else
            throw new NotImplementedException();
#endif
        }
    }
}
