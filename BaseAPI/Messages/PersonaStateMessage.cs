﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innovandalism.SteamWebApi.BaseAPI.Messages
{
    public class PersonaStateMessage : Message
    {
        int statusFlags;

        public int StatusFlags
        {
            get { return statusFlags; }
            set { statusFlags = value; }
        }

        SteamPersonaState personaState;

        public SteamPersonaState PersonaState
        {
          get { return personaState; }
          set { personaState = value; }
        }

        string personaName;

        public string PersonaName
        {
            get { return personaName; }
            set { personaName = value; }
        }
    }
}
