﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innovandalism.SteamWebApi.BaseAPI.Messages
{
    public class SayTextMessage : Message
    {
        string text;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }
    }
}
