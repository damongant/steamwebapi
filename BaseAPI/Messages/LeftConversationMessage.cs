﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innovandalism.SteamWebApi.BaseAPI.Messages
{
    public class LeftConversationMessage : Message
    {
        // Nothing to do here, just for the parser outside the API
    }
}
