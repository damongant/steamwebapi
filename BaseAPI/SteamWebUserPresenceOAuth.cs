﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Innovandalism.SteamWebApi.BaseAPI
{
    public class SteamWebUserPresenceOAuth : SteamWebInterface
    {
        public SteamWebUserPresenceOAuth(Steam parent) : base(parent) { }

        public override string InterfaceName
        {
            get
            {
                return "ISteamWebUserPresenceOAuth";
            }
        }

        public void Logon(string accessToken)
        {
#if DEBUG
            var p = new Dictionary<string, string>();
            p.Add("access_token", accessToken);
            p.Add("umqid", Engine.UMQID.ToString());

            var request = SetupRequest("Logon", 1, "POST", p);

            var response = DoRequest(request);
            if (response.Element("messagelast") != null)
            {
                Engine.LastMessageStr = response.Element("messagelast").Value;
            }
#else
            throw new NotImplementedException();
#endif
        }

        public PollResponse Poll(string accessToken)
        {
            var p = new Dictionary<string, string>();
            p.Add("access_token", accessToken);
            p.Add("umqid", Engine.UMQID.ToString());
            p.Add("message", Engine.LastMessageStr);

            var request = SetupRequest("Poll", 1, "POST", p);

            var response = DoRequest(request);
            #region DEBUGNSHIT
            FileStream log = new FileStream("log.txt", FileMode.Append);
            StreamWriter sw = new StreamWriter(log);
            sw.Write(response.ToString());
            sw.WriteLine("\n===========================");
            sw.Flush();
            log.Flush();
            sw.Close();
            log.Close();
            #endregion
            if (response.Element("messagelast") != null)
            {
                Engine.LastMessageStr = response.Element("messagelast").Value;
            }

            PollResponse pr = new PollResponse();

            List<Message> messages = new List<Message>();
            DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            foreach (var message in response.Elements("messages"))
            {
                Message m;

                var innerMessage = message.Element("message");
                MessageType type = Helpers.EnumConverters.ConvertMessageType(innerMessage.Element("type").Value);

                switch (type)
                {
                    case MessageType.SayText:
                        m = new Messages.SayTextMessage()
                        {
                            Text = innerMessage.Element("text").Value
                        };
                        break;
                    case MessageType.PersonaState:
                        m = new Messages.PersonaStateMessage()
                        {
                            StatusFlags = int.Parse(innerMessage.Element("status_flags").Value),
                            PersonaState = Helpers.EnumConverters.ConvertSteamUserState(int.Parse(innerMessage.Element("persona_state").Value)),
                            PersonaName = innerMessage.Element("persona_name").Value
                        };
                        break;
                    case MessageType.LeftConversation:
                        m = new Messages.LeftConversationMessage();
                        break;
                    case MessageType.Typing:
                        m = new Messages.TypingMessage();
                        break;
                    default:
                        throw new SteamWebException("invalid_message_type", "Detected an invalid/unknown message type.");
                }

                m.SteamIDFrom = long.Parse(innerMessage.Element("steamid_from").Value);
                m.Timestamp = DateTime.Now; // TODO: Fix this LAZY workaround (hey at least it's local time)
                m.Type = type;

                messages.Add(m);
            }
            pr.Messagebase = int.Parse(response.Element("messagebase").Value);
            pr.Messagelast = int.Parse(response.Element("messagelast").Value);
            pr.Messages = messages;
            pr.Timestamp = DateTime.Now; // TODO: OH AGAIN
            return pr;
        }

        public void Message(MessageType type, string dstSteamID)
        {
            Message(type, dstSteamID, null);
        }

        public void Message(MessageType type, string dstSteamID, string text)
        {
            var p = new Dictionary<string, string>();
            p.Add("access_token", Engine.AccessToken);
            p.Add("umqid", Engine.UMQID.ToString());
            p.Add("type", type.ToString().ToLower());
            p.Add("steamid_dst", dstSteamID);
            if (text != null)
                p.Add("text", System.Security.SecurityElement.Escape(text));

            var request = SetupRequest("Message", 1, "POST", p);
            var response = DoRequest(request);
        }
    }
}
