﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innovandalism.SteamWebApi.BaseAPI
{
    public enum MessageType
    {
        Unknown,
        Typing,
        SayText,
        PersonaState,
        LeftConversation
    }
}
