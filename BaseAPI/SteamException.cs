﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innovandalism.SteamWebApi.BaseAPI
{
    public class SteamWebException : Exception
    {
        private string code;

        public string Code
        {
            get { return code; }
        }

        public SteamWebException(string code, string message)
            : base(message)
        {
            this.code = code;
        }
    }
}
