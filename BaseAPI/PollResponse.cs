﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innovandalism.SteamWebApi.BaseAPI
{
    public class PollResponse
    {
        List<Message> messages;
        public List<Message> Messages
        {
            get { return messages; }
            set { messages = value; }
        }

        int messagelast;
        public int Messagelast
        {
            get { return messagelast; }
            set { messagelast = value; }
        }

        DateTime timestamp;
        public DateTime Timestamp
        {
            get { return timestamp; }
            set { timestamp = value; }
        }

        int messagebase;
        public int Messagebase
        {
            get { return messagebase; }
            set { messagebase = value; }
        }
    }
}
